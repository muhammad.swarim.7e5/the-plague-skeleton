package theplague.logic

import theplague.interfaces.IWorld
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.colony.Ant
import theplague.logic.colony.Colonization
import theplague.logic.colony.Dragon
import theplague.logic.vehicles.Bicycle
import theplague.logic.vehicles.Helicopter
import theplague.logic.vehicles.OnFoot
import theplague.logic.vehicles.Vehicle
import theplague.logic.weapons.Broom
import theplague.logic.weapons.Sword
import theplague.logic.weapons.Weapon
import kotlin.math.abs


class World(override val width: Int, override val height: Int) : IWorld {
    override var territories: List<List<Territory>> = MutableList(height) { MutableList(width) { Territory(0) } }
    private var actualTerritory = territories[width / 2][height / 2]
    override var player = Player()
    private var currentVehicle = 0

    init {
        player.positionNow(Position(width / 2, height / 2))
        actualTerritory.playerAdd(player)
    }

    private fun randomNumFun(randomNum: Int): Boolean {
        val response = List(100) { i -> i < randomNum }
        return response.shuffled().first()
        //mezcla aleatoriamente los elementos de la lista
    }


    override fun nextTurn() {
        player.turns += 1
        generateNewItems()
        if (player.currentVehicle !is OnFoot) currentVehicle += 1
        if (currentVehicle == 6) {
            player.currentVehicle = OnFoot(Int.MAX_VALUE, "\uD83D\uDEB6")
            currentVehicle = 0
            //el valor maximo de un Int
        }
        reproduce()
        createNewColonies()
        expand()
    }

    private fun reproduce() {
        for (i in territories) {
            for (j in i) {
                j.reproduce()
            }
        }
    }

    private fun itemCheck(position: Position): Boolean {
        val items = mutableListOf("\uD83D\uDEB2", "\uD83D\uDE81", "\uD83E\uDDF9", "\uD83D\uDDE1")
        val positions = territories[position.y][position.x].iconList()
        if (positions.isEmpty()) return false
        for (i in positions) {
            if (i.icon in items) return true
        }
        return false
    }

    private fun createNewColonies() {
        while (true) {
            val x = (0 until width).random()
            val y = (0 until height).random()
            val territory = territories[y][x]

            if (!territory.hasPlayer && territory.plagueSize == 0) {
                if (randomNumFun(30)) {
                    if (territory.colony == null) {
                        territory.colony = Ant(1, "\uD83D\uDC1C")
                    }
                } else if (randomNumFun(10)) {
                    if (territory.colony == null || territory.colony is Ant) {
                        territory.colony = Dragon(1, "\uD83D\uDC09")
                    }
                }
                territory.plagueSize = 1
                break
            }
        }
    }

    //se puede hacer mas corto
    private fun generateNewItems() {
        //  lista de rangos
        val rangs = mutableListOf((0 until 25), (25 until 35), (35 until 60), (60 until 70))
        // aqui lista de objetos que se pueden generar en el juego.
        val items = mutableListOf(
            Bicycle(5, "\uD83D\uDEB2"),
            Helicopter(5, "\uD83D\uDE81"),
            Broom(Int.MAX_VALUE, "\uD83E\uDDF9"),
            Sword(Int.MAX_VALUE, "\uD83D\uDDE1")
        )

        // random
        val numberRandom = (0 until 100).random()


        if (numberRandom in (0 until 70)) {

            lateinit var item: Iconizable
            for (i in rangs) {
                if (numberRandom in i) item = items[rangs.indexOf(i)]
            }


            var xPosition: Int
            var yPosition: Int
            do {
                xPosition = (territories[0].indices).random()
                yPosition = (territories.indices).random()
            } while (itemCheck(Position(xPosition, yPosition)))
            territories[yPosition][xPosition].addItem(item)
        }
    }

    override fun gameFinished(): Boolean {
        if (player.livesLeft == 0) {
            return true
        }
        return false
    }

    override fun canMoveTo(position: Position): Boolean {
        val actualPosition = player.getActualPosition()
        val maxDistance = when (player.currentVehicle.icon) {
            // en pie 1
            "\uD83D\uDEB6" -> 1
            // en bici 5
            "\uD83D\uDEB2" -> 5
            else -> return true
        }
        val distanceX = abs(actualPosition.x - position.x)
        val distanceY = abs(actualPosition.y - position.y)
        return distanceX <= maxDistance && distanceY <= maxDistance
    }


    override fun moveTo(position: Position) {
        actualTerritory.playerAdd(player)
        player.positionNow(position)
        actualTerritory = territories[position.y][position.x]
        actualTerritory.playerAdd(player)
    }

    override fun exterminate() {
        val position = player.getActualPosition()
        territories[position.y][position.x].exterminate(player.currentWeapon as Weapon)
    }

    override fun takeableItem(): Iconizable? {

        val position = player.getActualPosition()

        if (territories[position.y][position.x].iconList.isNotEmpty()) {

            for (i in territories[position.y][position.x].iconList) {
                when (i.icon) {
                    "\uD83D\uDEB2" -> {
                        return Vehicle(3, i.icon)
                    }

                    "\uD83D\uDE81" -> {
                        return Vehicle(3, i.icon)
                    }

                    "\uD83D\uDDE1" -> {
                        return Weapon(3, i.icon)
                    }

                    "\uD83E\uDDF9" -> {
                        return Weapon(3, i.icon)
                    }
                }
            }
        }
        return null
    }


    override fun takeItem() {
        val items = mutableListOf("\uD83D\uDEB2", "\uD83D\uDE81", "\uD83E\uDDF9", "\uD83D\uDDE1")
        val position = player.getActualPosition()
        val territori = territories[position.y][position.x]
        var icon: Iconizable = territori.iconList[0]
        for (i in territori.iconList) {
            if (i.icon in items) {
                if (i is Vehicle) currentVehicle = 0
                icon = i
                break } }
        when (icon.icon) {
            "\uD83D\uDEB2" -> { player.currentVehicle = Bicycle(5, icon.icon)
            }
            "\uD83D\uDE81" -> {
                player.currentVehicle = Helicopter(5, icon.icon)
            }
            "\uD83D\uDDE1" -> {
                player.currentWeapon = Sword(-1, icon.icon)
            }
            "\uD83E\uDDF9" -> {
                player.currentWeapon = Broom(-1, icon.icon)
            } }
        territories[position.y][position.x].iconList.remove(icon)

    }

    private fun expand() {
        for (row in territories.indices) {
            for (col in territories[row].indices) {
                val colony = territories[row][col].colony
                if (colony != null) {
                    when (colony) {
                        is Ant -> {
                            if (colony.needsToExpand()) {
                                val newColony = colony.expand(colony, Position(row, col), 10, 10)
                                place(newColony)
                                if (player.livesLeft > 0) {
                                    player.livesLeft-- }
                                break } }
                            is Dragon -> {
                            if (colony.needsToExpand()) {
                                val newColony = colony.expand(colony, Position(row, col), 10, 10)
                                place(newColony)
                                if (player.livesLeft > 0) {
                                    player.livesLeft-- }
                                break
                            } } } } } } }

    private fun place(colonization: Colonization) {
        val territory = territories[colonization.position.y][colonization.position.x]
        val colonyInTerritory = territory.colony

        if (colonyInTerritory != null) {
            if (colonyInTerritory is Ant) {
                val newColony = colonyInTerritory.colonizedBy(colonization.colony)
                territory.colony = newColony
            } else if (colonyInTerritory is Dragon) {
                val newColony = colonyInTerritory.colonizedBy(colonization.colony)
                territory.colony = newColony
            }
        } else {
            territory.colony = colonization.colony
        }
    }

}