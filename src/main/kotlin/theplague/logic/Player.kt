package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.vehicles.OnFoot
import theplague.logic.weapons.Hand

class Player : IPlayer, Iconizable{
    private var actualPosition = Position(0,0)
    fun positionNow (currentPosition: Position) {
        actualPosition = currentPosition
    }
    fun getActualPosition (): Position {
        return actualPosition
    }
    override var turns: Int = 1
    override var livesLeft: Int = 14
    override var currentWeapon: Iconizable = Hand(3, "\uD83D\uDC46")
    override var currentVehicle: Iconizable = OnFoot(1,"\uD83D\uDEB6")
    override val icon: String
        get() = "\uD83D\uDEB6"
}