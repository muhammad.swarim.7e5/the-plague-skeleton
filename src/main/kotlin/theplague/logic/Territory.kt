package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.logic.colony.Ant
import theplague.logic.colony.Colony
import theplague.logic.colony.Dragon
import theplague.logic.weapons.Weapon

class Territory (
    var plagueSize : Int
): ITerritory {
    var iconList = mutableListOf<Iconizable>()
    var hasPlayer = false
    var colony : Colony? = null
    fun playerAdd (icon: Iconizable) {
        if (!hasPlayer){
            hasPlayer = true
            iconList.add(icon)
        }
        else{
            hasPlayer = false
            iconList.remove(icon)
        }
    }
    fun addItem (item: Iconizable){
        iconList.add(item)
    }

    fun reproduce(){
        if (colony != null){
            when (colony){
                is Ant -> if ((colony as Ant).willReproduce()) {
                    (colony as Ant).reproduce()
                }
                is Dragon -> if ((colony as Dragon).willReproduce()) {
                    (colony as Dragon).reproduce()
                }
            }
        }
    }

    fun exterminate (weapon: Weapon){

        if (colony == null) return

        colony!!.attacked(weapon)

        if (colony!!.size <= 0) {
            colony = null
        }

    }
    override fun iconList(): List<Iconizable> {

        val icones: MutableList<Iconizable> = mutableListOf()

        for (i in iconList){
            if (i is Colony) {
                for (j in 0 until i.size) {
                    icones.add(Ant(1, i.icon))
                }
            }else icones.add(i)
        }

        if (colony != null){
            for (i in 0 until colony!!.size){
                icones.add(colony!!)
            }
        }
        return icones
    }

}