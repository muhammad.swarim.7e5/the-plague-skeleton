package theplague.logic.vehicles

import theplague.logic.weapons.Item

open class Vehicle(
    timesLeft: Int,
    icon: String) : Item(timesLeft, icon) {
    }