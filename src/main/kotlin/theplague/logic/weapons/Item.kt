package theplague.logic.weapons

import theplague.interfaces.Iconizable

abstract class Item  (
    val timesLeft : Int, override val icon: String
) : Iconizable{
    fun use () {

    }
}