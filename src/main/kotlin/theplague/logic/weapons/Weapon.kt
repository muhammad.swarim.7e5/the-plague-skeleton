package theplague.logic.weapons

open class Weapon(
    timesLeft: Int,
    icon: String) : Item(timesLeft, icon)