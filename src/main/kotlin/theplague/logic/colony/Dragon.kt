package theplague.logic.colony
import theplague.interfaces.Position
import theplague.logic.weapons.Broom
import theplague.logic.weapons.Hand
import theplague.logic.weapons.Weapon

class Dragon ( size: Int, override val icon: String) : Colony(size){
    private var timeToReproduce = 5

    override fun willReproduce(): Boolean {
        timeToReproduce -=1
        if (timeToReproduce == 0 && size < 3) {timeToReproduce = 4; return true}
        if (timeToReproduce == 0) timeToReproduce = 4
        return false
    }
    override fun reproduce() {
        size ++
    }
    override fun attacked(weapon: Weapon) {
        if (weapon is Hand || weapon is Broom) return
        size -= 1
    }

    override fun needsToExpand(): Boolean {
        return size == 3
    }
    override fun expand(colony: Colony, position: Position, height: Int, width: Int): Colonization {
        val x = (0 until width).random()
        val y = (0 until height).random()
        return Colonization(Dragon( 1,"\uD83D\uDC09"), Position(x, y ))
    }
    override fun colonizedBy(plague: Colony): Colony? {
        if (plague is Ant) return null
        if (!needsToExpand()){
            size ++
            return null
        }
        return plague
    } }