package theplague.logic.colony

import theplague.interfaces.Position
import theplague.logic.weapons.Broom
import theplague.logic.weapons.Sword
import theplague.logic.weapons.Hand
import theplague.logic.weapons.Weapon

class Ant(size: Int, override val icon: String) : Colony(size) {
    override fun willReproduce(): Boolean {
        return size < 3
    }
    override fun reproduce() {
        size ++
    }

    override fun attacked(weapon: Weapon) {
        when (weapon) {
            is Hand -> {
                size -= 2 }

            is Sword -> {
                size -= 1 }

            is Broom -> {
                size = 0 }
        }
    }
    override fun needsToExpand(): Boolean {
        return size == 3
    }
    override fun expand(colony: Colony, position: Position, height: Int, width: Int): Colonization {
        var x: Int
        var y: Int
        do{
            x = (-1..1).random()
            y = (-1..1).random()
        }while ((x + position.x) !in (0 until height) || (y + position.y) !in (0 until width) || (x == 0 && y == 0))

        return Colonization(Ant(1, "\uD83D\uDC1C"), Position(x + position.x, y + position.y))
    }
    override fun colonizedBy(plague: Colony): Colony? {
        if (plague is Dragon) {
            return plague
        }
        if (!needsToExpand()) {
            size++
        }
        return null
    }

}