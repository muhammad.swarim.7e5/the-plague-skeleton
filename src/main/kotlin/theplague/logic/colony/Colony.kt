package theplague.logic.colony

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.weapons.Weapon

abstract class Colony (var size : Int) : Iconizable {
    open fun willReproduce (): Boolean {
        return size <= 3
    }
    open fun reproduce (){
        if (willReproduce()) size ++
    }
    open fun needsToExpand (): Boolean {
        return false
    }
    open fun attacked (weapon: Weapon){

    }
    open fun colonizedBy(plague: Colony): Colony? {

        return Ant(1, "")

    }
    open fun expand (colony: Colony, position: Position, height: Int, width: Int): Colonization {
        return Colonization(Ant(1, "\uD83D\uDC1C"), Position(0, 0))
    }
}

class Colonization (val colony: Colony, val  position: Position)